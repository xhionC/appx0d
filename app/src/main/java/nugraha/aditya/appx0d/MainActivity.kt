package nugraha.aditya.appx0d

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), View.OnClickListener {

    val Collection = "students"
    val F_id  = "id"
    val F_name = "name"
    val F_alamat = "alamat"
    val F_nohp  = "nohp"
    var docid = ""
    lateinit var db  : FirebaseFirestore
    lateinit var allStudent : ArrayList <HashMap<String,Any>>
    lateinit var  adapter : SimpleAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        allStudent = ArrayList()
        insert.setOnClickListener(this)
        update.setOnClickListener(this)
        delete.setOnClickListener(this)

        lsData.setOnItemClickListener(itemClik)
    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(Collection).addSnapshotListener{ querySnapshot , e ->
            if(e !=null) Log.d("fireStore",e.message)
            showdata()
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.insert->{
                val hm = HashMap<String,Any>()
                hm.set(F_id,inid.text.toString())
                hm.set(F_name,name.text.toString())
                hm.set(F_alamat,alm.text.toString())
                hm.set(F_nohp,nohp.text.toString())
                db.collection(Collection).document(inid.text.toString()).set(hm).addOnSuccessListener {
                    Toast.makeText(this, "Data Berhasil di Tambahkan ", Toast.LENGTH_SHORT).show()
                    inid.setText("")
                    name.setText("")
                    alm.setText("")
                    nohp.setText("")
                }.addOnFailureListener { e ->
                    Toast.makeText(this, "Data Gagal di Tambahkan : ${e.message} ", Toast.LENGTH_SHORT).show()
                }



            }

            R.id.delete->{
                db.collection(Collection).whereEqualTo(F_id,docid).get().addOnSuccessListener {
                        results ->
                    for (doc in results){
                        db.collection(Collection).document(doc.id).delete()
                            .addOnSuccessListener {
                                inid.setText("")
                                name.setText("")
                                alm.setText("")
                                nohp.setText("")
                                Toast.makeText(this, "Data Berhasil di Hapus ", Toast.LENGTH_SHORT).show()
                            }.addOnFailureListener { e ->
                                Toast.makeText(this, "Data Gagal di Hapus : ${e.message} ", Toast.LENGTH_SHORT).show()
                            }
                    }
                }.addOnFailureListener { e ->
                    Toast.makeText(this, "Data Tidak Di Temukan : ${e.message} ", Toast.LENGTH_SHORT).show()
                }



            }

            R.id.update->{
                val hm = HashMap<String,Any>()
                hm.set(F_id,docid)
                hm.set(F_name,name.text.toString())
                hm.set(F_alamat,alm.text.toString())
                hm.set(F_nohp,nohp.text.toString())
                db.collection(Collection).document(docid).update(hm)
                    .addOnSuccessListener {
                        inid.setText("")
                        name.setText("")
                        alm.setText("")
                        nohp.setText("")
                        Toast.makeText(this, "Data Berhasil di Update ", Toast.LENGTH_SHORT).show()
                    }
                    .addOnFailureListener { e->
                        Toast.makeText(this, "Data Gagal di Update : ${e.message} ", Toast.LENGTH_SHORT).show()

                    }

            }
        }
    }

    fun showdata(){
        db.collection(Collection).get().addOnSuccessListener { result ->
            allStudent.clear()
            for(doc in result){
                val hm = HashMap<String,Any>()
                hm.set(F_id,doc.get(F_id).toString())
                hm.set(F_name,doc.get(F_name).toString())
                hm.set(F_alamat,doc.get(F_alamat).toString())
                hm.set(F_nohp,doc.get(F_nohp).toString())
                allStudent.add(hm)
            }
            adapter = SimpleAdapter(this ,allStudent,R.layout.raw_data,
                arrayOf(F_id,F_name,F_alamat,F_nohp),
                intArrayOf(R.id.txid,R.id.txname,R.id.txalamat,R.id.txnohp))
            lsData.adapter = adapter

        }
    }


    val itemClik = AdapterView.OnItemClickListener { parent, view, position, id ->
        val  hm = allStudent.get(position)
        docid = hm.get(F_id).toString()
        inid.setText(docid)
        name.setText(hm.get(F_name).toString())
        alm.setText(hm.get(F_alamat).toString())
        nohp.setText(hm.get(F_nohp).toString())
    }
}
